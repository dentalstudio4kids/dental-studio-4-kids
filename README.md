Dr. Denisse Lasanta, our founder, has created a dental practice exclusively for children from birth to 18 years of age. This is a practice where children and their unique needs during dental care are understood. Dr. Denisse and her staff speak both English and Spanish and so can comfort and reassure.

Address: 4675 Van Dyke Rd, Lutz, FL 33558, USA

Phone: 813-591-2200

Website: https://www.dentalstudio4kids.com
